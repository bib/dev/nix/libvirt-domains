# NOTE: This is not a generic libvirt wrapper ! This module has been written for
# a specific use case, and can not be used for generic libvirt domains.
{pkgs, lib, config, options, ...}:

with lib; with builtins; with strings; with attrsets;

let
  cfg = config.virtualisation.libvirtd;

  getOptionalAttr = name: default: set:
    if hasAttr name set then getAttr name set else default;
  getOptionalString = name: getOptionalAttr name "";
  defaultString = string: default: if (string != "") then string else default;

  byDomainType = domain: getOptionalString domain.type;
  byMachineChipset = machine: getOptionalString machine.chipset;


# libvirt domain XML generation
# =============================================================================
  domainXml = name: domain: let

    domainConfig = let

      # Generic XML generators
      # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      ifNotEmpty = text: optionalString (text != "");
      mkAttr = name: value: "${name}='${value}' ";
      mkShortTag = name: attrs: concatStrings [
        "<" name (ifNotEmpty attrs " ${attrs}") "/>"
      ];
      mkTag = name: attrs: value: if value != "" then concatStrings [
        "<" name (ifNotEmpty attrs " ${attrs}") ">"
          (toString value)
        "</" name ">"
      ] else (mkShortTag name attrs);
      mkOptionalTag = name: attrs: sentinel: value:
        optionalString (value != sentinel) (mkTag name attrs value);
      mkOptionalAttr = name: sentinel: value:
        optionalString (value != sentinel) (mkAttr name value);

    in let

      # libvirt XML generators
      # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      yesNo = value: if value then "yes" else "no";
      aliasConfig = alias: optionalString (alias != "") "<alias name='${alias}'/>";
      pciAddress' = domain: bus: slot: function:
        mkTag "address" (concatStrings [
          (mkAttr "type" "pci")
          (mkAttr "domain" domain) (mkAttr "bus" bus)
          (mkAttr "slot" slot) (mkAttr "function" function)
        ]) "";
      pciAddress = address:
        pciAddress' address.domain address.bus address.slot address.function;

    in let
      # Domain-level configuration generators
      # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

      # XML namespace/metadata
      # -----------------------------------------------------------------------
      xmlns = byDomainType domain {
        kvm = "xmlns:qemu='http://libvirt.org/schemas/domain/qemu/1.0'";
        lxc = "";
      };
      metadataConfig = let
        domainConfig = optionalString (domain.domain != "") ".${domain.domain}";
      in concatStrings [
        (mkTag "name" "" name)
        (mkTag "title" "" "${domain.hostname}${domainConfig}")
        (mkTag "uuid" "" "{{UUID}}")
        (mkTag "description" "" domain.description)
      ];

      # CPU configuration
      # -----------------------------------------------------------------------
      cpuConfig = let
        vcpu = domain.vcpu;
        cpuset = domain.cpuset;
        cputune = domain.cputune;
        vcpupins = cputune.vcpupins;
      in let
        cpusetConfig = mkOptionalAttr "cpuset" "" cpuset;
        vcpuConfig = mkOptionalTag "vcpu" "${cpusetConfig}placement='static'" 0 vcpu;
        vcpupinsConfig = concatStrings ( mapAttrsToList (vcpu: vcpuset:
          mkShortTag "vcpupin" (concatStrings [
            (mkAttr "vcpu" vcpu) (mkAttr "cpuset" (toString vcpuset))
          ] )
        ) vcpupins);
        cputuneConfig = mkTag "cputune" "" (concatStrings [
          (mkOptionalTag "shares" "" 0 cputune.shares)
          vcpupinsConfig
          cputune.extraConfig
        ] );
      in byDomainType domain {
        lxc = concatStrings [
          vcpuConfig
        ];
        kvm = concatStrings [
          ''<cpu mode='host-model' check='partial'>
            <model fallback='allow'/>
            <feature policy='disable' name='vmx'/>
          </cpu>''
          vcpuConfig cputuneConfig
        ];
      };

      # Memory configuration
      # -----------------------------------------------------------------------
      memoryConfig = concatStrings [
        (mkOptionalTag "memory" "unit='MiB'" 0 domain.memory)
        (byDomainType domain {
          kvm = mkOptionalTag "currentMemory" "unit='MiB" 0 domain.currentMemory;
        } )
      ];

      # Platform configuration
      # -----------------------------------------------------------------------
      osConfig = let
        os = domain.os;
      in byDomainType domain {
        kvm = let
          machineConfig = with os.machine; "${type}-${chipset}-${version}";
        in mkTag "os" "" (concatStrings [
          (mkTag "type" "arch='${os.arch}' machine='${machineConfig}'" "hvm")
          (mkShortTag "bootmenu" "enable='${yesNo os.bootmenu}' timeout='3000'")
          (mkShortTag "bios" "useserial='yes' rebootTimeout='0'")

          (os.extraConfig)
        ] );
        lxc = mkTag "os" "" (concatStrings [
          (mkTag "type" "arch='${os.arch}'" "exe")
          (mkTag "init" "" os.init)
        ] );
      };
      pmConfig = let
        pm = domain.pm;
      in byDomainType domain {
        kvm = ''<pm>
          <suspend-to-mem enabled='${yesNo suspendToMem}'/>
          <suspend-to-disk enabled='${yesNo suspendToDisk}'/>
        </pm>'';
      };
      featuresConfig = let
        features = domain.features;
      in byDomainType domain {
        kvm = ''<features>
          <vmport state='off'/>
          ${optionalString features.acpi "<acpi/>"}
          ${optionalString features.apic "<apic/>"}
          ${features.extraConfig}
        </features>'';
      };
      clockConfig = byDomainType domain {
        kvm = ''<clock offset='utc'>
          <timer name='rtc' tickpolicy='catchup'/>
          <timer name='pit' tickpolicy='delay'/>
          <timer name='hpet' present='no'/>
        </clock>'';
        lxc = ''<clock offset='utc'/>'';
      };

      # Emulator configuration
      # -----------------------------------------------------------------------
      libvirtConfig = with domain.libvirt; ''
        <on_poweroff>${onPoweroff}</on_poweroff>
        <on_reboot>${onReboot}</on_reboot>
        <on_crash>${onCrash}</on_crash>
      '';
      resourceConfig = let
        resource = domain.resource;
      in byDomainType domain {
        lxc = mkTag "resource" "" (concatStrings [
          (mkOptionalTag "partition" "" "" resource.partition)
          resource.extraConfig
        ] );
      };
      qemuConfig = let
        qemu = domain.qemu;
        qemuArgs = concatMapStrings (arg: "<qemu:arg value='${arg}'/>");
      in mkTag "qemu:commandline" "" (
        concatStrings [
          (optionalString (qemu.seabiosDebugLog != "") (qemuArgs [
            "-chardev" "file,id=seabios,path=${qemu.seabiosDebugLog}"
            "-device" "isa-debugcon,iobase=0x402,chardev=seabios" ] ) )
          (qemuArgs qemu.extraArgs)
        ]
      );

      # Device-level configuration generators
      # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

      devicesConfig = let

        # Device controllers
        # ---------------------------------------------------------------------
        controllerConfig = let
          pciController = ''
            <controller type='pci' index='0' model='pci-root'>
              ${aliasConfig "pci.0"}
            </controller>
          '';
          pciExpressController = ''
            <controller type='pci' index='0' model='pcie-root'>
              ${aliasConfig "pcie.0"}
            </controller>
            <controller type='pci' index='1' model='pcie-root-port'>
              <model name='pcie-root-port'/>
              <target chassis='1' port='0x8'/>
              ${aliasConfig "pci.1"}
              <address type='pci' multifunction='on'
               domain='0x0000' bus='0x00' slot='0x01' function='0x0'/>
            </controller>
            <controller type='pci' index='2' model='pcie-root-port'>
              <model name='pcie-root-port'/>
              <target chassis='2' port='0x9'/>
              ${aliasConfig "pci.2"}
              <address type='pci' multifunction='on'
               domain='0x0000' bus='0x00' slot='0x01' function='0x1'/>
            </controller>
          '';
          serialController = ''
            <controller type='virtio-serial' index='0'>
              ${aliasConfig "virtio-serial"}
              ${pciAddress' "0x0" "0x0" "0x1c" "0x0"}
            </controller>
          '';
          usbController = ''
            <controller type='usb' index='0' model='qemu-xhci' ports='8'>
              ${aliasConfig "usb0"}
              ${pciAddress' "0x0" "0x0" "0x1d" "0x0"}
            </controller>
          '';
          sataController = slot: func: ''
            <controller type='sata' index='0'>
              ${aliasConfig "sata0"}
              ${pciAddress' "0x0" "0x0" slot func}
            </controller>
          '';
          scsiController = ''
            <controller type='scsi' index='0' model='virtio-scsi' />
          '';
        in byDomainType domain {
          kvm = byMachineChipset domain.os.machine {
            q35 = concatStrings [
              pciExpressController
              (sataController "0x1f" "0x02")
              (usbController)
              (serialController)
              (scsiController)
            ];
            i440fx = concatStrings [
              pciController
              (sataController "0x1e" "0x00")
              (usbController)
              (serialController)
              (scsiController)
            ];
          };
        };

        # Serial, console and input
        # ---------------------------------------------------------------------
        serialConfig = byDomainType domain {
          kvm = ''<serial type='pty'>
            <target type='isa-serial' port='0'>
              <model name='isa-serial'/>
            </target>
            <alias name='serial0'/>
          </serial>'';
        };
        consoleConfig = byDomainType domain {
          kvm = ''<console type='pty'>
            <target type='serial' port='0'/> ${aliasConfig "console0"}
          </console>'';
          lxc = ''<console type='pty'>
            <target type='lxc' port='0'/> ${aliasConfig "console0"}
          </console>'';
        };
        inputConfig = byDomainType domain {
          kvm = ''
            <input type='mouse' bus='ps2'>${aliasConfig "input-mouse"}</input>
            <input type='keyboard' bus='ps2'>${aliasConfig "input-keyboard"}</input>
          '';
        };

        # Storage (disks/filesystems)
        # ---------------------------------------------------------------------
        storageConfig = let
          diskConfig = disk: let
            diskDriver = let driver = disk.driver; in ''
              <driver name='${driver.name}'
                      type='${driver.type}'
                      cache='${driver.cache}'/>
            '';
            diskSource = optionalString (disk.source.file != "") ''
              <source file='${disk.source.file}' />
            '';
            diskTarget = ''
              <target dev='${disk.target}' bus='${disk.busType}'/>
            '';
            diskBoot = optionalString disk.boot.enable ''
              <boot order='${toString(disk.boot.order)}'/>
            '';
            stringAddress = let address = disk.address; in {
              controller = toString(address.controller); bus = toString(address.bus);
              target = toString(address.target); unit = toString(address.unit);
            };
            diskAddress = with stringAddress;
              ''<address type='drive' controller='${controller}'
                 bus='${bus}' target='${target}' unit='${unit}'/>'';
            diskAlias = with stringAddress;
              ''<alias name='sata${controller}-${bus}-${target}-${unit}'/>'';
          in ''
            <disk type='file' device='${disk.type}'>
              ${optionalString disk.readonly "<readonly/>"}
              ${diskDriver}
              ${diskSource}
              ${diskTarget}
              ${diskBoot}
              ${diskAddress}
              ${diskAlias}
            </disk>
          '';
          fsConfig = fs: with builtins; ''
            <filesystem type='${fs.type}' accessmode='${fs.accessMode}'>
              <source dir='${fs.source.dir}'/>
              <target dir='${fs.target.dir}'/>
            </filesystem>
          '';
        in byDomainType domain {
          kvm = concatMapStrings diskConfig domain.disks;
          lxc = concatMapStrings fsConfig domain.filesystems;
        };

        # Networking interfaces
        # ---------------------------------------------------------------------
        networkConfig = let
          interfaceConfig = interface: byDomainType domain {
            kvm = ''
              <interface type='${interface.type}'>
                <mac address='${interface.macAddress}'/>
                <source bridge='${interface.source.bridge}'/>
                <model type='${interface.model}'/>
                ${aliasConfig interface.alias}
                ${pciAddress interface.address}
              </interface>
            '';
            lxc = ''
              <interface type='${interface.type}'>
                <mac address='${interface.macAddress}'/>
                <source bridge='${interface.source.bridge}'/>
                <guest dev='${interface.guest.dev}'/>
              </interface>
            '';
          };
        in concatMapStrings interfaceConfig domain.interfaces;

      # Devices configuration generator
      # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      in (concatStrings [
        (consoleConfig)
        (storageConfig)
        (networkConfig)
        (byDomainType domain {
          kvm = let
            agentConfig = ''
              <channel type='unix'>
              <target type='virtio' name='org.qemu.domain_agent.0' state='connected'/>
              ${aliasConfig "qemu-agent"}
              <address type='virtio-serial' controller='0' bus='0' port='1'/>
              </channel>
            '';
            balloonConfig = with domain.balloon; optionalString enable ''
              <memballoon model='virtio'>
              ${aliasConfig alias}
              ${pciAddress address}
              </memballoon>
            '';
          in concatStrings [
            controllerConfig inputConfig agentConfig balloonConfig
          ];
        })
        (domain.extraDevices)
      ] );

    # Domain configuration generator
    # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    in mkTag "domain"
      (concatStrings [(mkAttr "type" domain.type) xmlns])
      (concatStrings [
        # Generic configuration
        metadataConfig
        libvirtConfig
        osConfig
        cpuConfig
        memoryConfig
        clockConfig
        (byDomainType domain {
          kvm = concatStrings [
            qemuConfig
            featuresConfig
          ];
          lxc = concatStrings [
            resourceConfig
          ];
        } )
        (mkTag "devices" "" devicesConfig)
        # Extra configuration
        (domain.extraConfig)
      ] );

  # XML generation and validation
  # ---------------------------------------------------------------------------
  in let
    rawXml = pkgs.writeText "libvirt-domain-${name}.xml" domainConfig;
  in pkgs.runCommand "libvirt-domain-${name}-clean.xml" {} ''
    ${pkgs.libxml2}/bin/xmllint --exc-c14n ${rawXml} | \
    ${pkgs.libxml2}/bin/xmllint --nocompact --format - > $out
  '';

in {
  # libvirt domain option declaration
  # ===========================================================================

  options.virtualisation.libvirtd.domains = with lib; with types; let

    # Option generators
    # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    mkDiskOption = option: mkOption {
      default = {}; type = submodule { options = {
        enable = mkOption {
          default = option.enable;
          type = bool; example = true;
          description = "Whether to enable ${name}.";
        };
        boot = mkOption { default = {}; type = submodule { options = {
          enable = mkOption {
            default = option.boot.enable
              ; type = bool; example = true;
            description = "Whether to boot on ${name}.";
          };
          order = mkOption {
            type = int;
            default = option.boot.order;
          };
        };};};
        source = mkOption { type = str; default = ""; };
        readonly = mkOption { type = bool; default = option.readonly; };
      };};
    };

    mkLibvirtEventOption = name: default: with lib; with types; mkOption {
      default = default;
      example = "reboot";
      type = nullOr (enum [ "restart" "destroy" "poweroff" ]);
      description = "Action to apply on ${name}.";
    };

    pciAddressOption =
      mkOption { default = {}; type = submodule { options = {
        domain = mkOption { type =  str; default = "0x0"; };
        bus = mkOption { type =  str; default = "0x0"; };
        slot = mkOption { type =  str; };
        function = mkOption { type =  str; default = "0x0"; };
      };};};

    diskAddressOption =
      mkOption { default = {}; type = submodule { options = {
        controller = mkOption { type =  int; default = 0; };
        bus = mkOption { type =  int; default = 0; };
        target = mkOption { type =  int; default = 0; };
        unit = mkOption { type =  int; };
      };};};

  in mkOption {
    description = "static libvirt domains";
    default = {};
    type = attrsOf (submodule {
      options = {
        # Domain options
        # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

        # Virtualization type
        type = mkOption {
          type = nullOr (enum [ "kvm" "lxc" ]);
        };

        # [ALL] Machine metadata
        hostname = mkOption { type = str; };
        domain = mkOption { type = str; default = "local"; };
        description = mkOption { type = str; default = ""; };

        # [ALL] libvirt configuration
        libvirt = mkOption { default = {}; type = types.submodule { options = {
          onPoweroff = mkLibvirtEventOption "on poweroff" "destroy";
          onReboot = mkLibvirtEventOption "on reboot" "restart";
          onCrash = mkLibvirtEventOption "on crash" "restart";
          shutdownMethod = mkOption {
            type = nullOr (enum [ "" "shutdown" "managedsave" ]);
            default = "";
          };
          shutdownTimeout = mkOption {
            type = int;
            default = 15;
          };
        };};};

        # [LXC] Resources
        resource = mkOption { default = {}; type = submodule { options = {
          partition = mkOption { type = str; default = "/machine"; };
          extraConfig = mkOption { type = str; default = ""; };
        };};};

        # [ALL] Platform configuration
        os = mkOption { default = {}; type = submodule { options = {
          arch = mkOption { type = str; default = "x86_64"; };
          init = mkOption { type = str; default = "/bin/init"; };
          machine = mkOption { default = {}; type = submodule { options = {
            type = mkOption { type = str; default = "pc"; };
            chipset = mkOption {
              type = nullOr (enum [ "q35" "i440fx" ]); default = "q35";
            };
            version = mkOption { type = str; default = "4.0"; };
          };};};
          bootmenu = mkOption { type = bool; default = false; };
          extraConfig = mkOption { type = str; default = ""; };
        };};};

        # [KVM] Platform features
        features = mkOption { default = {}; type = submodule { options = {
          acpi = mkOption { type = bool; default = true; };
          apic = mkOption { type = bool; default = true; };
          extraConfig = mkOption { type = str; default = ""; };
        };};};

        # [KVM] Power management
        pm = mkOption { default = {}; type = submodule { options = {
          suspendToMem = mkOption { type = bool; default = true; };
          suspendToDisk = mkOption { type = bool; default = true; };
          extraConfig = mkOption { type = str; default = ""; };
        };};};

        # [ALL] CPU configuration
        vcpu = mkOption { type = int; default = 0; };
        cpuset = mkOption { type = str; default = ""; };
        cputune = mkOption { default = {}; type = submodule { options = {
          vcpupins = mkOption { default = {}; };
          shares = mkOption { type = int; default = 1024; };
          extraConfig = mkOption { type = str; default = ""; };
        };};};

        # [ALL] Memory configuration
        memory = mkOption { type = int; default = 0; };
        currentMemory = mkOption { type = int; default = 0; };

        # [KVM] Memory balloon
        balloon = mkOption { default = {}; type = submodule { options = {
          enable = mkEnableOption "memory balloon";
          address = pciAddressOption;
        };};};

        # [ALL] Network configuration
        interfaces = mkOption { type = listOf (submodule { options = {
          # [ALL] General options
          type = mkOption { type = str; default = "bridge"; };
          source = mkOption { type = submodule { options = {
            bridge = mkOption { type = str; default = ""; };
          };};};
          macAddress = mkOption { type = str; default = null; };
          # [KVM] Emulated model & PCI address
          model = mkOption { type = str; default = "virtio"; };
          address = pciAddressOption;
          alias = mkOption { type = str; default = ""; };
          # [LXC] Guest device config
          guest = mkOption { type = submodule { options = {
            dev = mkOption { type = str; default = "eth0"; };
          };};};
        };});};

        # [KVM] Storage configuration
        disks = mkOption { type = listOf (submodule { options = {
          type = mkOption {
            type = nullOr (enum [ "disk" "cdrom" ]);
            default = "disk";
          };
          busType = mkOption {
            type = nullOr (enum [ "sata" "ide" ]);
            default = "sata";
          };
          driver = mkOption { default = {}; type = submodule { options = {
            name = mkOption { type = str; default = "qemu"; };
            type = mkOption { type = str; default = "raw"; };
            cache = mkOption { type = str; default = "writethrough"; };
          };};};
          source = mkOption { default = {}; type = submodule { options = {
            file = mkOption { type = str; default = ""; };
          };};};
          target = mkOption { type = str; };
          readonly = mkOption { type = bool; default = false; };
          boot = mkOption { default = {}; type = submodule { options = {
            enable = mkEnableOption "booting on this disk";
            order = mkOption { type = int; default = 1; };
          };};};
          address = diskAddressOption;
        };});};

        # [LXC] Storage configuration
        filesystems = mkOption { type = listOf (submodule { options = {
          type = mkOption {
            type = nullOr (enum [ "mount" ]);
            default = "mount";
          };
          accessMode = mkOption {
            type = nullOr (enum [ "passthrough" ]);
            default = "passthrough";
          };
          source = mkOption { default = {}; type = submodule { options = {
            dir = mkOption { type = str; default = ""; };
          };};};
          target = mkOption { default = {}; type = submodule { options = {
            dir = mkOption { type = str; default = ""; };
          };};};
        };});};

        # [KVM] QEMU configuration
        qemu = mkOption { default = {}; type = submodule { options = {
          seabiosDebugLog = mkOption { type = str; default = ""; };
          extraArgs = mkOption { type = listOf str; default = []; };
        };};};

        # Extra configuration
        extraConfig = mkOption { type = str; default = ""; };
        extraDevices = mkOption { type = str; default = ""; };
      };
    } );
  };

  # systemd seprvice definition
  # ===========================================================================

  config = mkIf cfg.enable { systemd.services = lib.mapAttrs' (name: domain: let
     driver = getAttr domain.type { kvm = "qemu"; lxc = "lxc"; };
     virsh = "${pkgs.libvirt}/bin/virsh --connect=${driver}:///system";
    in lib.nameValuePair "libvirt-domain-${name}" {

      # Service configuration
      # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      description = "libvirt domain '${name}' (${domain.hostname})'";
      serviceConfig = { Type = "oneshot"; RemainAfterExit = "yes"; };
      after = [ "libvirtd.service" ];
      requires = [ "libvirtd.service" ];
      wantedBy = [ "multi-user.target" ];

      # Start script
      # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      script = let xml = domainXml name domain; in ''
        # Fetch the domain's UUID
        uuid="$(${virsh} domuuid '${name}' || true)"
        # Add the domain's UUID to the generated configuration
        ${virsh} define <(sed "s/{{UUID}}/$uuid/" '${xml}')
        # Start the domain
        ${virsh} start '${name}'
      '';

      # Stop script
      # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      preStop = let
        libvirt = domain.libvirt;
        shutdownMethod = defaultString libvirt.shutdownMethod (
          byDomainType domain { lxc = "shutdown"; kvm = "managedsave"; }
        );
      in ''
        # Attempt to shutdown gracefully
        ${virsh} ${shutdownMethod} '${name}' || ${virsh} shutdown '${name}' || true
        # Wait for the domain to shutdown
        let "timeout = $(date +%s) + ${toString(libvirt.shutdownTimeout)}"
        while [ "$(${virsh} list --name | \
                grep --count '^${name}$')" -gt   0 ]; do
          if [ "$(date +%s)" -ge "$timeout" ]; then
            ${virsh} destroy '${name}'
          else
            sleep 0.5
          fi
        done
      '';
    }
  ) cfg.domains; };
}
